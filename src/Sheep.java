
public class Sheep {

	enum Animal {
		sheep, goat
	};

	public static void main(String[] param) {

		Animal[] animals = new Animal[7]; // animals massiivis koosneb 7 loomast

		for (int i = 0; i < animals.length; i++) {
			if (Math.random() < 0.5) { // jaotab massiivi erinevad loomad
				animals[i] = Animal.goat; 

			} else {
				animals[i] = Animal.sheep;
			}
		}
		reorder(animals); // sorteerib sikud ja lambad

		

	}

	public static void reorder(Animal[] animals) {

		Animal[] goats = new Animal[animals.length]; // teeme sikkudele massiivi

		Animal[] sheeps = new Animal[animals.length]; // teeme lammastele
														// massiivi

		int count = 0; // sikkude arv massiivis
		int count2 = 0; // lammaste arv massiivis

		for (int i = 0; i < animals.length; i++) {
			
		if (animals[i] == Animal.goat) { //sikud kokku
			goats[count] = animals[i];
			count++;
		} 
		
		else {
			sheeps[count2] = animals[i]; //lambad kokku
			count2++;
		}
	}

		//teeme sorteeritud massiivi
		
		for (int i = 0; i < count; i++) {
			if (goats[i] != null)
				animals[i] = goats[i]; // jaotab massiivi esimesse poolde sikud
		}
		
		
		for (int j = 0; j < count2; j++) { 

			animals[count + j] = sheeps[j]; // teise poolde jäävad lambad, pärast sikkude arvu hakkavad lambad
		}

	}
}
